open Js_of_ocaml
open Js

type operations =
  { transactions : js_string t js_array t
  ; originations : js_string t js_array t
  ; reveals : js_string t js_array t
  ; delegations : js_string t js_array t }

val load_operation_hashes : Node.t -> string -> (operations, int) result Lwt.t

type header =
  (* Only the fields we are interested in *)
  < hash : js_string t readonly_prop
  ; level : int readonly_prop
  ; predecessor : js_string t readonly_prop
  ; timestamp : js_string t readonly_prop
  >

val load_header : Node.t -> string -> (header t, int) result Lwt.t

val confirm : Node.t -> from:int -> timeout:float -> js_string t -> [> `Found | `Timedout ] Lwt.t
