open Js_of_ocaml

module type S = sig
  open Js
  val decode : js_string t -> Typed_array.uint8Array t
  val encode : Typed_array.uint8Array t -> js_string t
end

(* 54 chars of "edsk..." in .tezos-client/secret_keys is actually
   an unencrypted Ed25519 32 byte seed. *)
module Seed : S

(* 96 chars of "edsk..." for Ed25519 64 byte secret key *)
module Secret_key : S

(* 54 chars of "edpk..." in .tezos-client/public_keys for Ed25519 32 byte public key *)
module Public_key : S

(* 36 chars of "tz1..." for Ed25519 public key hash *)
module Public_key_hash : S
