open Js_of_ocaml

let from_Some = Option.get

let from_Ok = Result.get_ok

let (!$) = Js.string
let (?$) = Js.to_string

module Console = struct
  let log a = Firebug.console##log a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

module Html = Dom_html

let document = Html.window##.document

let alert s = Html.window##alert (Js.string s)

exception No_element_id of string

let getById n =
  match Html.getElementById n with
  | exception Not_found -> raise (No_element_id n)
  | x -> x

let append_js_string parent s = Dom.appendChild parent @@ document##createTextNode s
let append_string parent s = append_js_string parent @@ Js.string s
let appendf parent fmt = Format.kasprintf (append_string parent) fmt
let append_p parent = Dom.appendChild parent @@ document##createElement (Js.string "p")
let append_json parent j = append_js_string parent (Js._JSON##stringify j)

let now () = Js.date##now

module Uint8Array = struct
  let of_string s =
    Typed_array.Bigstring.to_uint8Array (Bigstring.of_string s)

  let to_string a = Typed_array.String.of_uint8Array a
end

let is_worker () =
  try ignore (Js.Unsafe.eval_string "window"); false with _ -> true
