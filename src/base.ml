open Jsx

type address = Address of string

type mutez = Mutez of Z.t

module Mutez = struct
  let to_string (Mutez z) = "mutez " ^ Z.to_string z
end

let blake2b_32 s = Sodium.Crypto.generichash 32 (Uint8Array.of_string s)
