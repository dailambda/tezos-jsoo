open Js_of_ocaml

type primitive = string

type micheline

type t =
  | Int of Z.t
  | String of string
  | Bytes of bytes
  | Array of t list

  (* Some typical prims *)
  | Pair of t * t
  | Elt of t * t
  | Left of t
  | Right of t

  | OtherPrim of primitive * t list
  | Unknown

val pp : Format.formatter -> t -> unit

val decode : micheline Js.t -> t

val encode : t -> micheline Js.t
