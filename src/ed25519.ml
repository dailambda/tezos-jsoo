(* See tezos:src/lib_crypto/base58.ml *)
open Js_of_ocaml
open Js
open Jsx

module type S = sig
  val decode : js_string t -> Typed_array.uint8Array t
  val encode : Typed_array.uint8Array t -> js_string t
end

module Make(A : sig
    val prefix : Hex.t (* in hex *)
  end) : S = struct
  let unhex (`Hex h) = h
  let prefixbytes = String.length (unhex A.prefix) / 2
  let prefix = Uint8Array.of_string @@ Hex.to_string A.prefix
  let decode = Base58check.decode ~prefixbytes
  let encode s = Base58check.encode ~prefix s
end

module Seed = Make(struct let prefix = `Hex "0d0f3a07" end)
module Secret_key = Make(struct let prefix = `Hex "2bf64e07" end)
module Public_key = Make(struct let prefix = `Hex "0d0f25d9" end)
module Public_key_hash = Make(struct let prefix = `Hex "06a19f" end)
