include Lwt.Infix

let (>>?) at f =
  match at with
  | Error _ as e -> e
  | Ok v -> f v

let (>|?) at f =
  match at with
  | Error _ as e -> e
  | Ok v -> Ok (f v)

let (>>=?) at f =
  at >>= function
  | Error _ as e -> Lwt.return e
  | Ok x -> f x

let (>|=?) at f =
  at >|= function
  | Error _ as e -> e
  | Ok x -> Ok (f x)
