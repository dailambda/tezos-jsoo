open Js_of_ocaml
open Js_of_ocaml_lwt
open Base

type Error.t += HTTP of XmlHttpRequest.http_frame
             | JSON_parse of exn

type 'a contract_query = Node.t -> ?block:string -> address -> ('a, Error.t) result Lwt.t

val get : Node.t -> string -> ('a Js.t, Error.t) result Lwt.t

val balance : mutez contract_query
val counter : Z.t contract_query
val manager_key : string option contract_query

class type contract =
  let open Js in
  object
    method balance : js_string t readonly_prop
    method delegate : js_string t optdef readonly_prop
    method script : < > t optdef readonly_prop
    method counter : js_string t optdef readonly_prop
  end

val contract' : Node.t -> ?block:string -> address -> (contract Js.t, Error.t) result Lwt.t

val get_big_map :
  Node.t -> Z.t -> ('key -> string) -> 'key ->
  ('b -> ('c, Error.t) result) -> ('c option, Error.t) result Lwt.t

val get_storage :
  Node.t -> string -> ('a -> ('b, Error.t) result) -> ('b, Error.t) result Lwt.t
