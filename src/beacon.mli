open Js_of_ocaml
open Js

module BeaconMessageType : sig
  type t

  val make : string -> t

  val _PermissionRequest : t

  val _SignPayloadRequest : t

  val _OperationRequest : t

  val _BroadcastRequest : t

  val _PermissionResponse : t

  val _SignPayloadResponse : t

  val _OperationResponse : t

  val _BroadcastResponse : t

  val _Disconnect : t

  val _Error : t
end

class type beaconBaseMessage =
  object
    method id : js_string t readonly_prop

    method senderId : js_string t readonly_prop

    method type_ : BeaconMessageType.t readonly_prop

    method version : js_string t readonly_prop
  end

module PermissionScope : sig
  type t

  val make : string -> t

  val _SIGN : t

  val _OPERATION_REQUEST : t

  val _ENCRYPT : t

  val _THRESHOLD : t
end

module NetworkType : sig
  type t

  val make : string -> t

  val _MAINNET : t

  val _KATHMANDUNET : t

  val _GHOSTNET : t
end

class type network =
  object
    method name : js_string t optdef readonly_prop

    method rpcUrl : js_string t optdef readonly_prop

    method type_ : NetworkType.t readonly_prop
  end

val network :
  ?name: js_string t
  -> ?rpcUrl: js_string t
  -> type_: NetworkType.t
  -> unit
  -> network t

class type appMetadata =
  object
    method icon : js_string t optdef readonly_prop

    method name : js_string t readonly_prop

    method senderId : js_string t readonly_prop
  end

class type requestPermissionInput =
  object
    method network : network t optdef readonly_prop

    method scopes : PermissionScope.t js_array t optdef readonly_prop
  end

class type threshold =
  object
    method amount : js_string t readonly_prop

    method timeframe : js_string t readonly_prop
  end

class type permissionResponse =
  object
    method id : js_string t readonly_prop

    method network : network readonly_prop

    method publicKey : js_string t readonly_prop

    method scopes : PermissionScope.t js_array t readonly_prop

    method senderId : js_string t readonly_prop

    method threashold : threshold optdef readonly_prop

    method type_ : BeaconMessageType.t readonly_prop

    method version : js_string t readonly_prop
  end

module BeaconErrorType : sig
  type t

  val make : string -> t

  val _BROADCAST_ERROR : t

  val _NETWORK_NOT_SUPPORTED : t

  val _NO_ADDRESS_ERROR : t

  val _NO_PRIVATE_KEY_FOUND_ERROR : t

  val _NOT_GRANTED_ERROR : t

  val _PARAMETERS_INVALID_ERROR : t

  val _TOO_MANY_OPERATIONS : t

  val _TRANSACTION_INVALID_ERROR : t

  val _SIGNATURE_TYPE_NOT_SUPPORTED : t

  val _ABORTED_ERROR : t

  val _UNKNOWN_ERROR : t
end

class type errorResponse =
  object
    method errorData : Unsafe.any readonly_prop

    method errorType : BeaconErrorType.t readonly_prop

    method id : js_string t readonly_prop

    method senderId : js_string t readonly_prop

    method type_ : BeaconMessageType.t readonly_prop

    method version : js_string t readonly_prop
  end

class type dAppClientOptions =
  object
    method appUrl : js_string t optdef readonly_prop

    method disclaimerText : js_string t optdef readonly_prop

    method iconUrl : js_string t optdef readonly_prop

    method matrixNodes : js_string t js_array t optdef readonly_prop

    method name : js_string t readonly_prop

    method preferredNetwork : NetworkType.t optdef readonly_prop
  end

type dAppClient

class type permissionEntity =
  object
    method address : js_string t readonly_prop

    method network : network t readonly_prop

    method scopes : PermissionScope.t js_array t readonly_prop

    method threshold : threshold t optdef readonly_prop
  end

module Origin : sig
  type t

  val make : string -> t

  val _WEBSITE : t

  val _EXTENSION : t

  val _P2P : t
end

class type accountInfo =
  object
    method accountIdentifier : js_string t readonly_prop

    method address : js_string t readonly_prop

    method connectedAt : number t readonly_prop

    method network : network t readonly_prop

    method origin :
      < id : js_string t readonly_prop ; type_ : Origin.t readonly_prop > t readonly_prop

    method publicKey : js_string t readonly_prop

    method scopes : PermissionScope.t js_array t readonly_prop

    method senderId : js_string t readonly_prop

    method threshold : threshold t optdef readonly_prop
  end

module SigningType : sig
  type t

  val make : string -> t

  val _RAW : t

  val _OPERATION : t

  val _MICHELINE : t
end

class type requestSignPayloadInput =
  object
    method payload : js_string t readonly_prop

    method signingType : SigningType.t optdef readonly_prop

    method sourceAddress : js_string t optdef readonly_prop
  end

class type signPayloadResponse =
  object
    method id : js_string t readonly_prop

    method senderId : js_string t readonly_prop

    method signature : js_string t readonly_prop

    method signingType : SigningType.t readonly_prop

    method type_ : BeaconMessageType.t readonly_prop

    method version : js_string t readonly_prop
  end

type signPayloadResponseOutput = signPayloadResponse

module TezosOperationType : sig
  type t

  val make : string -> t

  val _ENDORSEMENT : t

  val _SEED_NONCE_REVELATION : t

  val _DOUBLE_ENDORSEMENT_EVIDENCE : t

  val _DOUBLE_BAKING_EVIDENCE : t

  val _ACTIVATE_ACCOUNT : t

  val _PROPOSALS : t

  val _BALLOT : t

  val _REVEAL : t

  val _TRANSACTION : t

  val _ORIGINATION : t

  val _DELEGATION : t
end

class type tezosBaseOperation =
  object
    method kind : TezosOperationType.t readonly_prop
  end

type michelineMichelsonV1Expression = Micheline.micheline

class type tezosTransactionParameters =
  object
    method entrypoint : js_string t readonly_prop

    method value : michelineMichelsonV1Expression t readonly_prop
  end

class type tezosTransactionOperation =
  object
    method amount : js_string t readonly_prop

    method counter : js_string t readonly_prop

    method destination : js_string t readonly_prop

    method fee : js_string t readonly_prop

    method gas_limit : js_string t readonly_prop

    method kind : TezosOperationType.t readonly_prop

    method parameters : tezosTransactionParameters t optdef readonly_prop

    method source : js_string t readonly_prop

    method storage_limit : js_string t readonly_prop
  end

class type partialTezosTransactionOperation =
  object
    method amount : js_string t readonly_prop

    method destination : js_string t readonly_prop

    method kind : TezosOperationType.t readonly_prop

    method parameters : tezosTransactionParameters t optdef readonly_prop
  end

class type tezosDelegationOperation =
  object
    method counter : js_string t readonly_prop

    method delegate : js_string t optdef readonly_prop

    method fee : js_string t readonly_prop

    method gas_limit : js_string t readonly_prop

    method kind : TezosOperationType.t readonly_prop

    method source : js_string t readonly_prop

    method storage_limit : js_string t readonly_prop
  end

class type partialTezosDelegationOperation =
  object
    method delegate : js_string t optdef readonly_prop

    method kind : TezosOperationType.t readonly_prop
  end

class type tezosOriginationOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.ORIGINATION *)
  method source : js_string t readonly_prop
  method fee : js_string t readonly_prop
  method counter : js_string t readonly_prop
  method gas_limit : js_string t readonly_prop
  method storage_limit : js_string t readonly_prop
  method balance : js_string t readonly_prop
  method delegate : js_string t optdef readonly_prop
  method script : js_string t readonly_prop
end

class type partialTezosOriginationOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.ORIGINATION *)
  method balance : js_string t readonly_prop
  method delegate : js_string t optdef readonly_prop
  method script : js_string t readonly_prop
end

type partialTezosOperation = tezosBaseOperation

class type requestOperationInput =
  object
    method operationDetails : partialTezosOperation t js_array t readonly_prop
  end

class type operationResponse =
  object
    method id : js_string t readonly_prop

    method senderId : js_string t readonly_prop

    method transactionHash : js_string t readonly_prop

    method type_ : BeaconMessageType.t readonly_prop

    method version : js_string t readonly_prop
  end

type operationResponseOutput = operationResponse

val new_DAppClient :
     name:js_string t
  -> ?iconUrl:js_string t
  -> ?appUrl:js_string t
  -> ?matrixNodes:js_string t js_array t
  -> ?preferredNetwork:NetworkType.t
  -> ?disclaimerText:js_string t
  -> unit
  -> dAppClient t

val requestPermissions :
     ?network:network t
  -> ?scopes:PermissionScope.t js_array t
  -> dAppClient t
  -> permissionResponse t Promise.t

val getActiveAccount : dAppClient t -> accountInfo t Promise.t

val setActiveAccount : dAppClient t -> accountInfo t -> unit Promise.t

val clearActiveAccount : dAppClient t -> unit Promise.t

val removeActiveAccount : dAppClient t -> js_string t -> unit Promise.t

val removeAllAccounts : dAppClient t -> unit Promise.t

val checkPermissions : dAppClient t -> BeaconMessageType.t -> bool Promise.t

val requestSignPayload :
  dAppClient t -> requestSignPayloadInput t -> signPayloadResponseOutput t Promise.t

val tezosTransactionParameters :
     entrypoint:js_string t
  -> value:michelineMichelsonV1Expression t
  -> tezosTransactionParameters t

val partialTezosOperation :
     < kind : < get : TezosOperationType.t ; .. > gen_prop ; .. > t
  -> partialTezosOperation t

val partialTezosTransactionOperation :
     mutez:int64
  -> destination:js_string t
  -> ?parameters:tezosTransactionParameters t
  -> unit
  -> partialTezosOperation t

val partialTezosDelegationOperation :
     delegate:js_string t option
  -> partialTezosOperation t

val partialTezosOriginationOperation :
     balance_mutez:int64
  -> delegate:js_string t option
  -> script:js_string t
  -> partialTezosOperation t

val requestOperation :
     dAppClient t
  -> partialTezosOperation t array
  -> operationResponseOutput t Promise.t
