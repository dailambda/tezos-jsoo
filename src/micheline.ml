open Js_of_ocaml

(*
export type MichelineMichelsonV1Expression =
  | { int: string }
  | { string: string } // eslint-disable-line id-blacklist
  | { bytes: string }
  | MichelineMichelsonV1Expression[]
  | {
      prim: MichelsonPrimitives
      args?: MichelineMichelsonV1Expression[]
      annots?: string[]
    }
*)

type micheline

let (!$) = Js.string

type primitive = string

type t =
  | Int of Z.t
  | String of string
  | Bytes of bytes
  | Array of t list

  (* Some typical prims *)
  | Pair of t * t
  | Elt of t * t
  | Left of t
  | Right of t

  | OtherPrim of primitive * t list
  | Unknown

let rec pp ppf =
  let open Format in
  function
  | Int i -> Z.pp_print ppf i
  | String s -> fprintf ppf "%S" s
  | Bytes b ->
      let `Hex h = Hex.of_bytes b in
      fprintf ppf "0x%s" h
  | Array xs ->
      fprintf ppf "{ %a }"
        (pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf ";@ ") pp) xs
  | Pair (t1, t2) ->
      fprintf ppf "(Pair %a %a)" pp t1 pp t2
  | Elt (t1, t2) ->
      fprintf ppf "(Elt %a %a)" pp t1 pp t2
  | Left t ->
      fprintf ppf "(Left %a)" pp t
  | Right t ->
      fprintf ppf "(Right %a)" pp t
  | OtherPrim (p, ts) ->
      fprintf ppf "(%s %a)"
        p
        (pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf "@ ") pp) ts
  | Unknown -> fprintf ppf "UNK"

let rec decode (json : _ Js.t) =
  let get_prop (json : _ Js.t) s =
    match Js.Unsafe.get json !$s with
    | x when x = Js.undefined -> None
    | x -> Some x
    | exception _ -> None
  in
  let get_prop_string json s : string option =
    Option.map Js.to_string @@ get_prop json s
  in
  match get_prop_string json "int" with
  | Some i -> Int (Z.of_string i)
  | None ->

  match get_prop_string json "string" with
  | Some s -> String s
  | None ->

  match get_prop_string json "bytes" with
  | Some s ->
      let bs = Hex.to_bytes (`Hex s) in
      Bytes bs
  | None ->

  match get_prop_string json "prim" with
  | Some prim ->
      begin match get_prop json "args" with
        | Some (args : _ Js.t Js.js_array Js.t) ->
            let args = List.map decode @@ Array.to_list @@ Js.to_array args in
            begin match prim, args with
            | "Pair", xs ->
                let rec f = function
                  | [] | [_] -> Unknown
                  | [a; b] -> Pair (a, b)
                  | x::xs -> Pair (x, f xs)
                in
                f xs
            | "Elt", [a; b] -> Elt (a, b)
            | "Left", [a] -> Left a
            | "Right", [a] -> Right a
            | _ -> OtherPrim (prim, args)
            end
        | None | exception _ -> Unknown
      end
  | None ->
      (* can be array? *)
      match get_prop json "length" with
      | Some _ ->
          let a : _ Js.t Js.js_array Js.t = Obj.magic json in
          Array (Array.to_list @@ Array.map decode @@ Js.to_array a)
      | None ->
          Unknown

let rec encode t =
  let obj = Js.Unsafe.obj in
  let prim n args =
    obj [|"prim", Js.Unsafe.inject !$n;
          "args", Js.Unsafe.coerce (Js.array (Array.of_list (List.map encode args))) |]
  in
  match t with
  | Array ts ->
      Js.Unsafe.coerce (Js.array (Array.of_list (List.map encode ts)))
  | Int z ->
      obj [|"int", Js.Unsafe.inject !$(Z.to_string z)|]
  | String s ->
      obj [|"string", Js.Unsafe.inject !$s|]
  | Bytes b ->
      let `Hex hex = Hex.of_bytes b in
      obj [|"bytes", Js.Unsafe.inject !$hex|]
  | Pair (a, b) -> prim "Pair" [ a; b ]
  | Elt (a, b) -> prim "Elt" [ a; b ]
  | Left a -> prim "Left" [ a ]
  | Right a -> prim "Right" [ a ]
  | OtherPrim (p, args) -> prim p args
  | Unknown -> assert false
