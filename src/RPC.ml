open Js_of_ocaml
open Js_of_ocaml_lwt
open Jsx
open Misc
open Infix
open Base

module Path = struct
  let head = "/chains/main/blocks/head"
  let contract ?(block=head) c = block ^/ "context/contracts" ^/ c
end

let url node path =
  from_Some @@ Url.url_of_string @@ node.Node.base_url ^ path

type Error.t += HTTP of XmlHttpRequest.http_frame
             | JSON_parse of exn

let () =
  let open Format in
  Error.register_printer @@ fun ppf -> function
  | HTTP http_frame -> fprintf ppf "HTTP %d" http_frame.code; true
  | JSON_parse exn -> fprintf ppf "JSON parse error: %s" (Printexc.to_string exn); true
  | _ -> false

let get node path =
  let url = url node path in
  (* The node must be CORS enabled.
     See https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
  *)
  XmlHttpRequest.perform url
  >|= fun http_frame ->
  match http_frame.code with
  | 200 ->
      begin try
          Ok (Js._JSON##parse !$(http_frame.content))
        with exn -> Error (JSON_parse exn)
      end
  | _ -> Error (HTTP http_frame)

(* 404 is not an error but None *)
let get_opt node path =
  get node path >|= function
  | Error (HTTP http_frame) when http_frame.code = 404 -> Ok None
  | Ok x -> Ok (Some x)
  | Error _ as e -> e

let contract n node ?block (Address c) =
  get node (Path.contract ?block c ^/ n)

let _contract_opt n node ?block (Address c) =
  get_opt node (Path.contract ?block c ^/ n)

type 'a contract_query = Node.t -> ?block:string -> address -> ('a, Error.t) result Lwt.t

let balance node ?block a =
  contract "balance" node ?block a >|=?
  fun (s : Js.js_string Js.t) -> Mutez (Z.of_string (Js.to_string s))

let counter node ?block a =
  contract "counter" node ?block a
  >|=? fun (s : Js.js_string Js.t) -> Z.of_string (Js.to_string s)

let manager_key node ?block a =
  contract "manager_key" node ?block a
  >|=? fun (s : Js.js_string Js.t Js.Opt.t) ->
  Option.map Js.to_string @@ Js.Opt.to_option s

class type contract =
  let open Js in
  object
    method balance : js_string t readonly_prop
    method delegate : js_string t optdef readonly_prop
    method script : < > t optdef readonly_prop
    method counter : js_string t optdef readonly_prop
  end

let contract' node ?block (Address c) =
  (get node (Path.contract ?block c) : (contract Js.t, Error.t) result Lwt.t)

let script_expr packer v =
  let s = packer v in
  let h = blake2b_32 s in
  Base58check.encode
    ~prefix:(Uint8Array.of_string (Hex.to_string (`Hex "0d2c401b"))) h

(* Ugh, RPC call per one int *)
let get_big_map node big_map_id key_packer key of_micheline =
  let open Lwt.Syntax in
  let url =
    "/chains/main/blocks/head/context/big_maps/"
    ^ Z.to_string big_map_id
    ^ "/"
    ^ Js.to_string (script_expr key_packer key)
  in
  let+ res = get node url in
  match res with
  | Ok res -> Result.map (fun x -> Some x) @@ of_micheline res
  | Error (HTTP {code= 404; _}) -> Ok None
  | Error e -> Error e

(* Ugh, RPC call per one int *)
let get_storage node contract of_micheline =
  let open Lwt.Syntax in
  let url =
    "/chains/main/blocks/head/context/contracts/"
    ^ contract
    ^ "/storage"
  in
  let+ res = get node url in
  match res with
  | Ok res -> of_micheline res
  | Error e -> Error e
