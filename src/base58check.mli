open Js_of_ocaml
open Js

module Obsolete : sig
  (** returns a hex string *)
  val decode : prefixbytes:int -> js_string t -> [`Hex of js_string t]

  (** prefix and the returned value are hex strings *)
  val encode : prefix:[< `Hex of js_string t] -> [< `Hex of js_string t] -> js_string t
end

val decode : prefixbytes:int -> js_string t -> Typed_array.uint8Array t

val encode : prefix:Typed_array.uint8Array t -> Typed_array.uint8Array t -> js_string t
