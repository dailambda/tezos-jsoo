open Js_of_ocaml
open Js

class type keypair = object
  method publicKey : Typed_array.uint8Array t readonly_prop
  method privateKey : Typed_array.uint8Array t readonly_prop
end

module Crypto : sig
  val sign_seed_keypair : Typed_array.uint8Array t -> keypair t

  (** Blake2B hash *)
  val generichash :
    int -> Typed_array.uint8Array t -> Typed_array.uint8Array t

  val sign_detached :
    mes:Typed_array.uint8Array t ->
    sk:Typed_array.uint8Array t -> Typed_array.uint8Array t
  val sign_verify_detached :
    sign:Typed_array.uint8Array t ->
    mes:Typed_array.uint8Array t -> pk:Typed_array.uint8Array t -> bool t
end
