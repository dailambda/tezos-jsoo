module Let_syntax = Lwt.Infix.Let_syntax

val ( >>= ) : 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t
val ( >|= ) : 'a Lwt.t -> ('a -> 'b) -> 'b Lwt.t
val ( <&> ) : unit Lwt.t -> unit Lwt.t -> unit Lwt.t
val ( <?> ) : 'a Lwt.t -> 'a Lwt.t -> 'a Lwt.t
val ( =<< ) : ('a -> 'b Lwt.t) -> 'a Lwt.t -> 'b Lwt.t
val ( =|< ) : ('a -> 'b) -> 'a Lwt.t -> 'b Lwt.t

val (>>?) : ('a, 'b) result -> ('a -> ('c, 'b) result) -> ('c, 'b) result
val (>|?) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result
val ( >>=? ) :
  ('a, 'b) result Lwt.t ->
  ('a -> ('c, 'b) result Lwt.t) -> ('c, 'b) result Lwt.t
val ( >|=? ) :
  ('a, 'b) result Lwt.t -> ('a -> 'c) -> ('c, 'b) result Lwt.t
