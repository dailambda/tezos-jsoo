type t = ..

(** Install a printer for error type [t].

    The function returns [true] when it supports the given error.
    Otherwise it must return [false], so that the other registered
    printers can try printing it.
*)
val register_printer : (Format.formatter -> t -> bool) -> unit

val pp : Format.formatter -> t -> unit
