open Js_of_ocaml
open Js

class type keypair = object
  method publicKey : Typed_array.uint8Array t readonly_prop
  method privateKey : Typed_array.uint8Array t readonly_prop
end

class type sodium = object
  method crypto_sign_seed_keypair_ : Typed_array.uint8Array t -> keypair t meth
  method crypto_generichash_ : int -> Typed_array.uint8Array t -> Typed_array.uint8Array t meth
  method crypto_sign_detached_ : mes:Typed_array.uint8Array t -> sk:Typed_array.uint8Array t -> Typed_array.uint8Array t meth
  method crypto_sign_verify_detached_ : sign:Typed_array.uint8Array t -> mes:Typed_array.uint8Array t -> pk:Typed_array.uint8Array t -> bool t meth
end

module Crypto = struct

  let sodium () : sodium Js.t =
    Js.Unsafe.js_expr "sodium" (* need to be evaluated each time? *)

  let sign_seed_keypair x = (sodium ())##crypto_sign_seed_keypair_ x
  let generichash x y = (sodium ())##crypto_generichash_ x y
  let sign_detached ~mes ~sk = (sodium ())##crypto_sign_detached_ ~mes ~sk
  let sign_verify_detached ~sign ~mes ~pk =
    (sodium ())##crypto_sign_verify_detached_ ~sign ~mes ~pk
end
