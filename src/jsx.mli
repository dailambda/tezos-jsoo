open Js_of_ocaml

val from_Some : 'a option -> 'a

val from_Ok : ('a, _) result -> 'a

(** [alert] of Js *)
val alert : string -> unit

(* wrapper of [getElementById] *)
val getById : string -> Dom_html.element Js.t

(** Equivalent of [Js.string] *)
val (!$) : string -> Js.js_string Js.t

(** Equivalent of [Js.to_string] *)
val (?$) : Js.js_string Js.t -> string

module Console : sig
  val log : 'a -> unit

  val logf : ('a, Format.formatter, unit, unit) format4 -> 'a
end

(** The document *)
val document : Dom_html.document Js.t

(** Append strings to the specified Dom element *)
val append_js_string : #Dom.node Js.t -> Js.js_string Js.t -> unit
val append_string : #Dom.node Js.t -> string -> unit
val append_json : #Dom.node Js.t -> < .. > Js.t -> unit
val appendf : #Dom.node Js.t -> ('a, Format.formatter, unit, unit) format4 -> 'a
(** Append <p> to the specified Dom element *)
val append_p : #Dom.node Js.t -> unit

(** Milliseconds! *)
val now : unit -> float

val is_worker : unit -> bool

module Uint8Array : sig
  val of_string : string -> Typed_array.uint8Array Js.t
  val to_string : Typed_array.uint8Array Js.t -> string
end
