open Js_of_ocaml
open Js

module Obsolete = struct
  let decode ~prefixbytes:(prefixbytes:int) (b58c : js_string t) : [`Hex of js_string t] =
    let hex = (Js.Unsafe.js_expr "base58check.decode" : _ -> _) b58c in
    `Hex (hex##slice_end (prefixbytes * 2))

  (* takes Js strings of hex *)
  let encode ~prefix:(`Hex prefix) (`Hex data) =
    (Js.Unsafe.js_expr "base58check.encode" : js_string t -> js_string t -> js_string t)
      data prefix
end

let decode
    ~prefixbytes:(prefixbytes:int) (b58c : js_string t)
  : Typed_array.uint8Array t =
  let x = (Js.Unsafe.js_expr "bs58check.decode" : _ -> _) b58c in
  x##slice_end prefixbytes

(* takes Js strings of hex *)
let encode ~prefix data =
  let len_prefix = prefix##.length in
  let len_data = data##.length in
  let buf = new%js Typed_array.uint8Array (len_prefix + len_data) in
  buf##set_fromTypedArray prefix 0;
  buf##set_fromTypedArray data len_prefix;
  (Js.Unsafe.js_expr "bs58check.encode" : _ -> _) buf
