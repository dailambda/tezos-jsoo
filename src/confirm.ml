open Js_of_ocaml
open Js_of_ocaml_lwt
open Js
open Jsx

type operations =
  { transactions : js_string t js_array t
  ; originations : js_string t js_array t
  ; reveals : js_string t js_array t
  ; delegations : js_string t js_array t }

type raw_operations = js_string t js_array t js_array t

let operations_of_raw_operations ros =
  match Js.to_array ros with
  | [| transactions; originations; reveals; delegations |] ->
      Some { transactions; originations; reveals; delegations }
  | _ -> None

let http_json_perform url =
  let open Lwt.Syntax in
  let+ http_frame = XmlHttpRequest.perform url in
  match http_frame.code with
  | 200 -> Ok (Js._JSON##parse (Js.string http_frame.content))
  | code -> Error code

let load_operation_hashes node block =
  let open Lwt.Syntax in
  let url =
    Option.get
    @@ Url.url_of_string
    @@ node.Node.base_url ^ "/chains/main/blocks/" ^ block ^ "/operation_hashes"
  in
  let+ res = http_json_perform url in
  match res with
  | Ok (json : raw_operations) ->
      begin match operations_of_raw_operations json with
      | Some ops -> Ok ops
      | None -> Error 999
      end
  | Error e -> Error e

(*
{ "protocol": "PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg",
  "chain_id": "NetXnHfVqm9iesp",
  "hash": "BMbSfwTDJHAzLG6hG15rSkzANYoGS8rjAGyYwpcqccZsQZv2rVU",
  "level": 1502535, "proto": 4,
  "predecessor": "BKptmLkR99rUfuyxgV6ej9cuKYyyFbnyaDLAhXfeYwdoNHWi2bL",
  "timestamp": "2022-11-15T01:51:55Z", "validation_pass": 4,
  "operations_hash": "LLoaJfWGpQjVnfkpP2zPPXu4VDixZ4Zeepa1Ls3k243QLfC473Tut",
  "fitness": [ "02", "0016ed47", "", "ffffffff", "00000000" ],
  "context": "CoViMrtdYhQsW7EA8xV8G548fghdm7PANqTdqQ4ece3M5A5xMk61",
  "payload_hash": "vh2Ti6bpD7ENaA2irw8shQf9K35sxrpEgr4hAX6qJqrK8W6oznmm",
  "payload_round": 0, "proof_of_work_nonce": "8408476f57080300",
  "liquidity_baking_toggle_vote": "on",
  "signature":
    "sigjr7f7ro38SgJFcqD8KkFMyeWJACeQCb5bAZkaxuURtwo7Jp2m7bgHjq286PPh7h79Gcnrh34b6JC5ovvzyUXAXpNvijtN" }
*)

type header =
  (* Only the fields we are interested in *)
  < hash : js_string t readonly_prop
  ; level : int readonly_prop
  ; predecessor : js_string t readonly_prop
  ; timestamp : js_string t readonly_prop
  >

let load_header node block =
  let open Lwt.Syntax in
  let url =
    Option.get
    @@ Url.url_of_string
    @@ node.Node.base_url ^ "/chains/main/blocks/" ^ block ^ "/header"
  in
  let+ res = http_json_perform url in
  match res with
  | Ok (json : header t) -> Ok json
  | Error e -> Error e

let confirm node ~from ~timeout oh =
  Console.logf "confirming %s is on the chain..." (Js.to_string oh);
  let started = now () in
  let open Lwt.Syntax in
  let rec check_blocks checked header =
    if header##.level = from then Lwt.return_ok (`Not_found checked)
    else if List.mem header##.hash checked then
      (* All the predecessors are already checked *)
      Lwt.return_ok (`Not_found checked)
    else begin
      Console.logf "checking block %s" (Js.to_string header##.hash);
      let* res = load_operation_hashes node (Js.to_string header##.hash) in
      match res with
      | Ok { transactions; originations; reveals; delegations } ->
          if
            Array.mem oh (Js.to_array transactions)
            || Array.mem oh (Js.to_array originations)
            || Array.mem oh (Js.to_array reveals)
            || Array.mem oh (Js.to_array delegations) then
            Lwt.return_ok `Found
          else
            let checked = header##.hash :: checked in
            let* res = load_header node (Js.to_string header##.predecessor) in
            begin match res with
            | Ok header -> check_blocks checked header
            | Error e -> Lwt.return_error (e, checked)
            end
      | Error e -> Lwt.return_error (e, checked)
    end
  in
  let rec loop checked =
    if Jsx.now () -. started > timeout *. 1000.0 then begin
      Console.logf "Confirmation of %s timedout" (Js.to_string oh);
      Lwt.return `Timedout
    end else
      let* res = load_header node "head" in
      match res with
      | Error e ->
          Console.logf "load header %d" e;
          let* () = Lwt_js.sleep 10.0 in
          loop checked
      | Ok header ->
          let* res = check_blocks checked header in
          match res with
          | Ok `Found ->
              Console.logf "confirmed %s" (Js.to_string oh);
              Lwt.return `Found
          | Ok (`Not_found checked) ->
              (* no blocks have oh so far *)
              let* () = Lwt_js.sleep 10.0 in
              loop checked
          | Error (e, checked) ->
              (* no blocks have oh so far *)
              Console.logf "error %d" e;
              let* () = Lwt_js.sleep 10.0 in
              loop checked
  in
  loop []
