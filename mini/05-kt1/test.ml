open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

module Html = Dom_html

(* Append the format result to "result" HTML element *)
let report fmt =
  let elem = Html.getElementById "result" in
  Format.kasprintf
    (fun s ->
       elem##.innerText := elem##.innerText##concat (Js.string (s ^ "\n")))
    fmt

(* The node.  It must be CORS enabled. *)
let node = "https://mainnet.smartpy.io"

let url address =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ address

module Micheline = struct
  (* Micheline, AST for Michelson *)
  type t =
    | Int of Z.t
    | String of string
    | Bytes of string
    | Seq of t list
    | Prim of { prim: string; args: t list; annots: string list }

  let pp_list sep_fmt f xs =
    Format.pp_print_list ~pp_sep:(fun ppf () -> Format.fprintf ppf sep_fmt) f xs

  let pp ppf =
    let one_liner = function
      | Int _ | String _ | Bytes _ -> true
      | Prim {prim=_; args=[]; annots=[]} -> true
      | _ -> false
    in
    let rec pp ppf = function
      | Int z -> Z.pp_print ppf z
      | String s -> Format.fprintf ppf "%S" s
      | Bytes b -> Format.fprintf ppf "%S" b
      | Prim { prim; args= []; annots } ->
          Format.fprintf
            ppf
            "%s%s"
            prim
            (if annots = [] then "" else String.concat " " ("" :: annots))
      | Prim { prim; args; annots } ->
          if List.for_all one_liner args then
            Format.fprintf
              ppf
              "%s%s @[<h>%a@]"
              prim
              (if annots = [] then "" else String.concat " " ("" :: annots))
              (pp_list "@ " (fun ppf x ->
                   let parens =
                     match x with
                     | Int _ | String _ | Bytes _ | Prim { prim=_; args= []; annots=[] } | Seq _
                       ->
                         false
                     | _ -> true
                   in
                   if parens then Format.fprintf ppf "(%a)" pp x else pp ppf x))
              args
          else
            Format.fprintf
              ppf
              "%s%s @[<v>%a@]"
              prim
              (if annots = [] then "" else String.concat " " ("" :: annots))
              (pp_list "@ " (fun ppf x ->
                   let parens =
                     match x with
                     | Int _ | String _ | Bytes _ | Prim {prim=_; args=[]; annots= []} | Seq _
                       ->
                         false
                     | _ -> true
                   in
                   if parens then Format.fprintf ppf "(%a)" pp x else pp ppf x))
              args
      | Seq [] -> Format.fprintf ppf "{ }"
      | Seq nodes ->
          Format.fprintf
            ppf
            "@[<v>{ %a@ }@]"
            (pp_list "@,; " pp)
            nodes
    in
    pp ppf

  open Js

  (* Abstract type for JSON value of Micheline *)
  type json

  (* { "prim":$prim, "args":$args, "annots":$annots } *)
  type prim =
    < prim : js_string t readonly_prop;
      args : json t js_array t optdef readonly_prop;
      annots : js_string t js_array t optdef readonly_prop >

  (*
        { /* Int */
          "int": $bignum }
        || { /* String */
             "string": $unistring }
        || { /* Bytes */
             "bytes": /^([a-zA-Z0-9][a-zA-Z0-9])*$/ }
        || [ $micheline.012-Psithaca.michelson_v1.expression ... ]
        /* Sequence */
        || { /* Prim__generic
                Generic primitive (any number of args with or without
                annotations) */
             "prim": $012-Psithaca.michelson.v1.primitives,
             "args"?: [ $micheline.012-Psithaca.michelson_v1.expression ... ],
             "annots"?: [ string ... ] },
  *)
  let parse_json =
    let key_int = string "int" in
    let key_string = string "string" in
    let key_bytes = string "bytes" in
    let case j k a b = Optdef.case (Unsafe.get j k) b a in
    let rec f (j : json t) =
      try Seq (Array.to_list (Array.map f (to_array (Unsafe.coerce j))))
      with
      | _ ->
          case j key_int
            (fun (j : js_string t) -> Int (Z.of_string (to_string j)))
          @@ fun () ->
          case j key_string
            (fun (j : js_string t) -> String (to_string j))
          @@ fun () ->
          case j key_bytes
            (fun (j : js_string t) -> Bytes (to_string j))
          @@ fun () ->
             (* must be prim *)
             let p = (Unsafe.coerce j : prim t) in
             let prim = to_string p##.prim in
             let args =
               Optdef.case p##.args
                 (fun () -> [])
                 (fun a -> Array.to_list (Array.map f (to_array a)))
             in
             let annots =
               Optdef.case p##.annots
                 (fun () -> [])
                 (fun a -> Array.to_list (Array.map to_string (to_array a)))
             in
             Prim { prim; args; annots }
    in
    f

end

open Js

type script =
  < code : Micheline.json t readonly_prop;
    storage : Micheline.json t readonly_prop >

type contract =
  < balance : js_string t readonly_prop;
    delegate : js_string t optdef readonly_prop;
    script : script t optdef readonly_prop;
    counter : js_string t optdef readonly_prop >

let query_contract address =
  let+ http_frame = XmlHttpRequest.perform (url address) in
  match http_frame.code with
  | 200 ->
      (* Parse the string as an JSON of type contract *)
      let c : contract Js.t = Js._JSON##parse (Js.string http_frame.content)
      in
      let pp_js_string_t ppf x = Format.pp_print_string ppf (Js.to_string x) in
      let pp_optdef f ppf x = Js.Optdef.case x (fun () -> Format.fprintf ppf "none") (f ppf) in
      let pp_script ppf script =
        let storage = Micheline.parse_json script##.storage in
        let code = Micheline.parse_json script##.code in
        Format.fprintf ppf "@[storage: %a;@ code: %a@]" Micheline.pp storage Micheline.pp code
      in
      report "%s : @[<v>balance: %a;@ delegate: %a;@ counter: %a;@ script: %a@]"
        address
        pp_js_string_t c##.balance
        (pp_optdef pp_js_string_t) c##.delegate
        (pp_optdef pp_js_string_t) c##.counter
        (pp_optdef pp_script) c##.script
  | _ ->
      report "HTTP code %d" http_frame.code

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ query_contract "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9";
      ignore @@ query_contract "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N";
      Js._false)
