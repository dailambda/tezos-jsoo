open Js_of_ocaml
open Tezos_jsoo

(* This example is not practical since it handles a secret key.
   For security, the signing must be done in a wallet. *)

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

let f () =
  let secret_seed = Base58check.decode ~prefixbytes:4 !$secret_seed_b58c in
  Console.log secret_seed;
  (* ED25519 keypair is generated from a seed.
     keypair is a JS object with publicKey and privateKey fields.
  *)
  let keypair = Sodium.Crypto.sign_seed_keypair secret_seed in
  Console.log keypair;
  let secret_key = keypair##.privateKey in
  let public_key = keypair##.publicKey in
  (* message to sign *)
  let mes = Uint8Array.of_string "hello" in
  Console.log mes;
  (* sign with the secret key *)
  let sign = Sodium.Crypto.sign_detached ~mes ~sk:secret_key in
  (* Tezos Base58check encoded signature *)
  Console.logf "signature: %s" @@ (?$)
  @@ Base58check.encode
    ~prefix:(Uint8Array.of_string (Hex.to_string (`Hex "09f5cd8612")))
    sign;
  (* Check the signature with the public key *)
  assert (Js.to_bool @@ Sodium.Crypto.sign_verify_detached ~sign ~mes ~pk:public_key);
  Console.logf "signature successfully checked"

module Html = Dom_html

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ f ();
      Js._false)
