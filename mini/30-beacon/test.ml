open Js_of_ocaml
open Tezos_jsoo

module Html = Dom_html

let f () =
  let client = Beacon.new_DAppClient ~name: !$"TezosJSOO" () in
  Console.log client;
  (* Beacon uses JS promises.  We use promise_jsoo *)
  let open Promise.Syntax in

  Promise.catch ~rejected:(fun err -> Console.log err; Promise.return ())
  @@
  let* res =
    Beacon.requestPermissions client
      ?network:None
      ?scopes:None
  in
  Console.log res;
  let* ainfo = Beacon.getActiveAccount client in
  Console.log ainfo;
  (* Send 1 mutez to myself *)
  let* opresp =
    Beacon.requestOperation client
      [| Beacon.partialTezosTransactionOperation
           ~mutez: 1L
           ~destination:ainfo##.address ()
      |]
  in
  Console.log opresp;
  Promise.return ()

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ f ();
      Js._false)
