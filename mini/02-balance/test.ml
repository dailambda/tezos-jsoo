open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

(* The node.  Must be CORS enabled. *)
let node = "https://mainnet.smartpy.io"

(* Tezos RPC path to get the balance of an address *)
let balance_url adrs =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ adrs ^ "/balance"

let adrs = "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9"

module Html = Dom_html

let start () =
  let elem = Html.getElementById "result" in

  (* printf style reporting function to textarea *)
  let report fmt =
    Format.kasprintf
      (fun s ->
         elem##.innerText := elem##.innerText##concat (Js.string (s ^ "\n")))
      fmt
  in

  let url = balance_url adrs in
  report "URL: %s" (Url.string_of_url url);
  (* The node must be CORS enabled. *)
  let+ http_frame = XmlHttpRequest.perform (balance_url adrs) in
  match http_frame.code with
  | 200 ->
      report "response: %S" http_frame.content;
      (* It is a JSON string.  Parse it using Js._JSON##parse *)
      let s : Js.js_string Js.t =
        Js._JSON##parse (Js.string http_frame.content)
      in
      (* balance in Tezos is an arbitrary-precision integer, in mutez
         (micro tez).

         To use module Z of Zarith library, we use zarith_stubs_js package
         to provide JS code for Z.t.  See dune file.
      *)
      let balance = Z.of_string (Js.to_string s) in
      (* let's print the balance in tz *)
      let tz, mtz = Z.div_rem balance (Z.of_int 1_000_000) in
      report "%s : %s.%06d TEZ" adrs (Z.to_string tz) (Z.to_int mtz)
  | _ ->
      report "HTTP code %d" http_frame.code

let _ =
  Html.window##.onload := Html.handler (fun _ -> ignore @@ start (); Js._false)
