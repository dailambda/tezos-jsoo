open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

(* The node.  It must be CORS enabled. *)
let node = "https://mainnet.smartpy.io"

(* Tezos RPC path to get the latest block info *)
let url = Option.get @@ Url.url_of_string (node ^ "/chains/main/blocks/head")

module Html = Dom_html

let start () =
  (* Node access via XmlHttpRequest.

     The node must be CORS enabled. See:
     https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
  *)
  let+ http_frame = XmlHttpRequest.perform url in (* let+ = monadmic map *)
  let s =
    match http_frame.code with
    | 200 -> http_frame.content
    | _ -> Printf.sprintf "HTTP code %d" http_frame.code
  in
  let elem = Html.getElementById "result" in
  elem##.innerText := Js.string s

let _ =
  (* Call [start] when the page is loaded *)
  Html.window##.onload :=
    Html.handler (fun _ -> ignore @@ start (); Js._false)
