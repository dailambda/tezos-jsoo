open Js_of_ocaml
open Tezos_jsoo

module Html = Dom_html

(* This example is not practical since it handles a secret key.
   For security, the signing must be done in a wallet. *)

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

module Sodium = struct
  open Js_of_ocaml
  open Js

  class type keypair = object
    method publicKey : Typed_array.uint8Array t readonly_prop
    method privateKey : Typed_array.uint8Array t readonly_prop
  end

  class type sodium = object
    method crypto_sign_seed_keypair_ : Typed_array.uint8Array t -> keypair t meth
    method crypto_generichash_ : int -> Typed_array.uint8Array t -> Typed_array.uint8Array t meth
    method crypto_sign_detached_ : mes:Typed_array.uint8Array t -> sk:Typed_array.uint8Array t -> Typed_array.uint8Array t meth
    method crypto_sign_verify_detached_ : sign:Typed_array.uint8Array t -> mes:Typed_array.uint8Array t -> pk:Typed_array.uint8Array t -> bool t meth
  end

  type t = sodium
end

(* sodium library functions are given in the sodium JS object *)
let sodium : Sodium.sodium Js.t =
  Js.Unsafe.js_expr "sodium" (* need to be evaluated each time? *)

let f () =
  Console.log sodium;
  let secret_seed =
    Base58check.decode ~prefixbytes: 4 !$secret_seed_b58c
  in
  Console.log secret_seed;
  (* ED25519 keypair is generated from a seed.
     keypair is a JS object with publicKey and privateKey fields.
  *)
  let keypair = sodium##crypto_sign_seed_keypair_ secret_seed in
  Console.log keypair;
  let public_key = keypair##.publicKey in
  (* Tezos public key is Base58Check-encoded with prefix "0d0f25d9" *)
  let public_key_b58c =
    Base58check.encode ~prefix:(Uint8Array.of_string (Hex.to_string (`Hex "0d0f25d9"))) public_key
  in
  Console.logf "public_key: %s" ?$public_key_b58c;
  assert (?$public_key_b58c = "edpkvWHGruAVQzq7jYaG6ywXFRwP7mV4twm9noGGexDr8kHudrH5Fn");
  (* tz1.. is created from 20 byte Blake2b hash of the public key. *)
  let pkhash = sodium##crypto_generichash_ 20 public_key in
  (* Base58Check prefix of Tezos ED25519 public key hashes is "06a19f" *)
  let pkhash_b58c =
    Base58check.encode ~prefix:(Uint8Array.of_string (Hex.to_string (`Hex "06a19f")))
      pkhash
  in
  Console.logf "public_key_hash: %s" ?$pkhash_b58c;
  assert (?$pkhash_b58c = "tz1ScC5AEZQUbKBieU6F4ovpvNLVXufBK2gL")

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ f ();
      Js._false)
