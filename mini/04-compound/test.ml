open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

module Html = Dom_html

(* Append the format result to "result" HTML element *)
let report fmt =
  let elem = Html.getElementById "result" in
  Format.kasprintf
    (fun s ->
       elem##.innerText := elem##.innerText##concat (Js.string (s ^ "\n")))
    fmt

(* The node.  It must be CORS enabled. *)
let node = "https://mainnet.smartpy.io"

(* This time, we get more complex data of a contract *)
let url address =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ address

(* Type for the data we skip parsing this time (script) *)
type unknown

(* The data returned from the RPC is an JSON object, specified at
   https://tezos.gitlab.io/active/rpc.html#get-block-id-context-contracts-contract-id

   Here is the OCaml object type for the JS object isomorphic to it.
*)
open Js
type contract =
  <  (* balance : required string field *)
     balance : js_string t readonly_prop;

     (* delegate : optional string field *)
     delegate : js_string t optdef readonly_prop;

     (* script : optional.  We ignore the contents for now *)
     script : unknown t optdef readonly_prop;

     (* counter : optional string field *)
     counter : js_string t optdef readonly_prop
  >

let query_contract address =
  let+ http_frame = XmlHttpRequest.perform (url address) in
  match http_frame.code with
  | 200 ->
      (* Parse the string as an JSON of type contract *)
      let c : contract Js.t = Js._JSON##parse (Js.string http_frame.content)
      in
      report "%s : @[<v>balance: %s;@ delegate: %s;@ script: %s;@ counter: %s@]"
        address
        (Js.to_string c##.balance)
        (Js.Optdef.case c##.delegate (fun () -> "none") Js.to_string)
        (Js.Optdef.case c##.script (fun () -> "none") (fun x ->
             (* It is an abstract value in OCaml,
                but we can print it using _JSON##stringify *)
             (Js.to_string (Js._JSON##stringify x))))
        (Js.Optdef.case c##.counter (fun () -> "none") Js.to_string)
  | _ ->
      report "HTTP code %d" http_frame.code

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ query_contract "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9";
      ignore @@ query_contract "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N";
      Js._false)
