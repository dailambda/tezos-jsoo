open Js_of_ocaml

(* We use Tezos_jsoo library *)
open Tezos_jsoo

module Html = Dom_html

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

(* Base58Check de/encoders.
   They call functions defined in js/base58check.js. *)
module Base58check = struct
  open Js

  (* returns Js string of hex *)
  let decode ~prefixbytes b58c =
    let hex = (Js.Unsafe.js_expr "base58check.decode" : _ -> _) b58c in
    hex##slice_end (prefixbytes * 2)

  (* takes Js strings of hex *)
  let encode ~prefix data =
    (Js.Unsafe.js_expr "base58check.encode" : js_string t -> js_string t -> js_string t) data prefix
end

let f () =
  (* ED25519 seed is Base58Check encoded with 4 bytes prefix 0d0f3a07.
     The prefix is defined in src/lib_crypto/base58.ml of Tezos source code:

       [let ed25519_seed = "\013\015\058\007" (* edsk(54) *)]

     Some useful prefixes:

       Ed25519 public_key edpk.. : "0d0f25d9"
       Ed25519 signature edsig.. : "09f5cd8612"
  *)
  (* Once decoded, the 4 byte prefix must be removed *)
  let secret_seed = Base58check.decode ~prefixbytes:4 (Js.string secret_seed_b58c) in
  Console.log secret_seed;
  (* Encode back with the prefix *)
  let secret_seed_b58c' = Base58check.encode ~prefix:!$"0d0f3a07" secret_seed in
  Console.log secret_seed_b58c';
  assert (secret_seed_b58c = Js.to_string secret_seed_b58c')

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ f ();
      Js._false)
