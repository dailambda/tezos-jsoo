utils = {
    to_hex : function (args) {
	var ret = "";
	for ( var i = 0; i < args.length; i++ )
            ret += (args[i] < 16 ? "0" : "") + args[i].toString(16);
	return ret; //.toUpperCase();
    },

    of_hex : function (str) {
	if (typeof str == 'string') {
            var ret = new Uint8Array(Math.floor(str.length / 2));
            var i = 0;
            str.replace(/(..)/g, function(str) { ret[i++] = parseInt(str, 16);});
            return ret;
	}
    }
}
