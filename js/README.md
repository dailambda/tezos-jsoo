# Javascript libraries

## bs58check.js

```
$ npm install bs58check
$ npx browserify bs58check_gen.js -o bs58check.js
```

## sodium.js

<https://github.com/jedisct1/libsodium.js>

Available at <https://raw.githubusercontent.com/jedisct1/libsodium.js/master/dist/browsers/sodium.js>

## walletbeacon.min.js

<https://github.com/airgap-it/beacon-sdk/> version 3.3.0

```
$ npm install @airgap/beacon-sdk
$ cp node_modules//@airgap/beacon-sdk/dist/walletbeacon.min.js .
```

## utils.js

Hex converters
