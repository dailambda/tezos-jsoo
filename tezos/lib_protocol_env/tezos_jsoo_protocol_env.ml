include Tezos_stdlib

module Error_monad = struct
  include Tezos_error_monad.Error_monad
  let ok x = Ok x
end

include Error_monad
module Micheline = struct
  include Tezos_micheline.Micheline
  include Tezos_micheline.Micheline_encoding
end

module Lwtreslib = Tezos_lwt_result_stdlib.Lwtreslib.Bare
include Lwtreslib

module Bounded = Tezos_base.Bounded
module Time = Tezos_base.Time.Protocol
module Operation = Tezos_base.Operation
module Block_header = Tezos_base.Block_header
module Fitness = Tezos_base.Fitness

module Signature = Tezos_crypto.Signature
include Tezos_crypto.Hashed
module Base58 = Tezos_crypto.Base58
module Blake2B = Tezos_crypto.Blake2B
module S = Tezos_crypto.Intfs
type error = Tezos_error_monad.Error_monad.error = ..
type 'a tzresult = ('a, error list) result
let tzfail e = Lwt.return_error [e]
let register_error_kind = Error_monad.register_error_kind
let ok x = Ok x
let return x = Lwt.return_ok x
let error e = Error [e]
let (>>?) = Result.bind
let (>|?) x f = Result.map f x
let (>>=?) = Lwt_result.bind

include Tezos_crypto.Signature

module Data_encoding = struct
  include Data_encoding
  let string = string'
  module Fixed = struct
    include Fixed
    let bytes = bytes'
    let string = string'
  end
  module Variable = struct
    include Variable
    let bytes = bytes'
    let string = string'
  end
  module Bounded = struct
    include Bounded
    let bytes = bytes'
    let string = string'
  end

    (* TODO: https://gitlab.com/nomadic-labs/data-encoding/-/issues/58
       Remove when fix is integrated in data-encoding. *)
    let splitted ~json ~binary =
      let open Data_encoding__.Encoding in
      let e = splitted ~json ~binary in
      {
        e with
        encoding =
          (match e.encoding with
          | Splitted {encoding; json_encoding; _} ->
              Splitted
                {
                  encoding;
                  json_encoding;
                  is_obj = is_obj json && is_obj binary;
                  is_tup = is_tup json && is_tup binary;
                }
          | desc -> desc);
      }
end

module Dummy = struct
  type t = Data_encoding.Json.t
  let encoding = Data_encoding.Json.encoding
  let pp = Data_encoding.Json.pp
end

module Dal = struct
  type parameters = {
    redundancy_factor : int;
    page_size : int;
    slot_size : int;
    number_of_shards : int;
  }

  let parameters_encoding =
    let open Data_encoding in
    conv
      (fun {redundancy_factor; page_size; slot_size; number_of_shards} ->
        (redundancy_factor, page_size, slot_size, number_of_shards))
      (fun (redundancy_factor, page_size, slot_size, number_of_shards) ->
        {redundancy_factor; page_size; slot_size; number_of_shards})
      (obj4
         (req "redundancy_factor" uint8)
         (req "page_size" uint16)
         (req "slot_size" int31)
         (req "number_of_shards" uint16))
end

(*
module Constants_parametric_repr = Dummy

module Dal = struct
  include Dummy

  type parameters = Data_encoding.Json.t
  let parameters_encoding = Data_encoding.Json.encoding

  module Commitment = Dummy
end

module Dal_operations_repr = struct
  include Dummy
  module Publish_slot_header = Dummy
end

module Sc_rollups = struct
  module Kind = Dummy
end

module Sc_rollup_repr = struct
  include Dummy
  module Hash = Dummy
  module Staker = Dummy
  module Address = Dummy
end

module Sc_rollup_game_repr = struct
  type refutation = Data_encoding.Json.t
  let refutation_encoding = Data_encoding.Json.encoding
  module Index = Dummy
end

module Sc_rollup_commitment_repr = struct
  include Dummy
  module Hash = Dummy
end

module Sc_rollup_proof_repr = struct
  type serialized = Data_encoding.Json.t
  let serialized_encoding = Data_encoding.Json.encoding
end

module Plonk = struct
  type public_parameters = Data_encoding.Json.t
  let public_parameters_encoding = Data_encoding.Json.encoding
end

module Zk_rollup_account_repr = struct
  module SMap = struct
    type _ t = Data_encoding.Json.t
  end
  let circuits_info_encoding = Data_encoding.Json.encoding
end

module Zk_rollup_state_repr = Dummy

module Zk_rollup_repr = struct
  include Dummy
  module Address = Dummy
end

module Zk_rollup_operation_repr = Dummy
module Zk_rollup_ticket_repr = Dummy
module Zk_rollup_update_repr = Dummy

module Seed_repr = struct
  type nonce = Data_encoding.Json.t
  let nonce_encoding = Data_encoding.Json.encoding
  type vdf_solution = Data_encoding.Json.t
  let vdf_solution_encoding = Data_encoding.Json.encoding
  let compare_vdf_solution : vdf_solution -> vdf_solution -> int = Stdlib.compare
end
*)

let failwith = Stdlib.failwith
